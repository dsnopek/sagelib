#!/usr/bin/env python

# SAGElib Designer - The Graphical Game Design Platform
# Copyright 2002 David Snopek
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#

#
# Description: my happy animation editor
#

from wxPython.wx import *
from Media.animation2d import Document, Controller, EditorPanel
from Media.manager import Media
from Media.dialogs import FileDescriptorDialog, NameDialog
from Media.scroll2dmedia import Definition

# file menu
ID_NEW    = 101
ID_OPEN   = 102
ID_SAVE   = 103
ID_SAVEAS = 104
ID_PRINT  = 105
ID_EXIT   = 106
# edit menu
ID_UNDO = 201
ID_REDO = 202
# animation menu
ID_ADDFRAME    = 301
ID_REMOVEFRAME = 302
ID_MOVEFRAME   = 303
ID_PLAY        = 304

class AnimEditor(wxFrame):
	def __init__(self, parent, standalone, pos, size):
		wxFrame.__init__(self, parent, -1, "Animation Editor", pos, size)
		
		# make a status bar
		self.CreateStatusBar()
		self.SetStatusText("Welcome to the Animation Editor")
		
		# build the file menu
		FileMenu = wxMenu()
		if standalone:
			FileMenu.Append(ID_NEW, "&New", "Create a new animation")
			FileMenu.Append(ID_OPEN, "&Open", "Open a file")
		FileMenu.Append(ID_SAVE, "&Save", "Save your changes to the file")
		if standalone:
			FileMenu.Append(ID_SAVEAS, "Save &As", "Save to a different file")
		FileMenu.AppendSeparator()
		FileMenu.Append(ID_PRINT, "&Print", "Print a story board of the animation")
		FileMenu.AppendSeparator()
		FileMenu.Append(ID_EXIT, "E&xit", "Exit the animation editor")
		# build the edit menu
		EditMenu = wxMenu()
		EditMenu.Append(ID_UNDO, "&Undo", "Undo last operation")
		EditMenu.Append(ID_REDO, "&Redo", "Redo a previouly undone operation")
		# build the animation menu
		AnimMenu = wxMenu()
		AnimMenu.Append(ID_ADDFRAME, "&Add Frame", "Loads a new image and adds it the animation")
		AnimMenu.Append(ID_REMOVEFRAME, "&Remove Frame", "Removes a frame from the animtion")
		AnimMenu.AppendSeparator()
		AnimMenu.Append(ID_PLAY, "&Play Animation", "Starts the animation player")
		
		# build the menubar
		menubar = wxMenuBar()
		menubar.Append(FileMenu, "&File")
		menubar.Append(EditMenu, "&Edit")
		menubar.Append(AnimMenu, "&Animation")
		self.SetMenuBar(menubar)
		
		# integrate events
		EVT_MENU(self, ID_NEW, self.OnFileNew)
		EVT_MENU(self, ID_OPEN, self.OnFileOpen)
		EVT_MENU(self, ID_SAVE, self.OnFileSave)
		EVT_MENU(self, ID_SAVEAS, self.OnFileSaveAs)
		EVT_MENU(self, ID_EXIT, self.OnFileExit)
		EVT_MENU(self, ID_UNDO, self.OnUndo)
		EVT_MENU(self, ID_REDO, self.OnRedo)
		EVT_MENU(self, ID_ADDFRAME, self.OnAddFrame)
		EVT_MENU(self, ID_REMOVEFRAME, self.OnRemoveFrame)
		
		# create the document controller and media object
		self.Filename = ""
		self.Descriptor = ""
		self.Controller = Controller()
		self.Media = Media(Definition)
		
		# add a animation editor panel
		self.Editor = EditorPanel(self, -1, wxPoint(0,0), wxSize(200,200))
		self.Controller.AddView(self.Editor)
		sizer = wxBoxSizer(wxHORIZONTAL)
		sizer.Add(self.Editor, 1, wxEXPAND)
		self.SetSizer(sizer)
		self.SetAutoLayout(True)

	def OutputError(self, message, title):
		wxMessageBox(message, title, wxOK | wxICON_EXCLAMATION, self)
	
	def QueryModified(self):
		"""
		This function returns true if we should carry with the operation and
		false if it was canceled.  The save operation will be done automatically
		"""
		
		if self.Controller.Modified != False:
			result = wxMessageBox("The current animation has been modified.  Do you wish to save?", "Modified", wxYES_NO | wxCANCEL, self)
			if result == wxCANCEL:
				return False
			elif result == wxYES:
				self.OnFileSave(None)
		return True
	
	def OnFileNew(self, event):
		if not self.QueryModified():
			return
		
		self.Filename = ""
		self.Descriptor = ""
		self.Media.Clear()
		self.Controller.CreateNew()
	
	def OnFileOpen(self, event):
		if not self.QueryModified():
			return
		
		dlg = FileDescriptorDialog(self, -1, "Open animation ...", self.Filename, ".", "", wxOPEN, self.Media)
		if dlg.ShowModal() == wxID_OK:
			self.Filename = dlg.GetPath()
			
			# if a media object was created when the dialog was doing a Descriptor
			# lookup then we should try and reuse it.  Make sure that the file
			# names match!
			if dlg.Media and dlg.Media.Filename == self.Filename:
				self.Media = dlg.Media
			else:
				self.Media.LoadFile(self.Filename)
			
			if dlg.GetDescriptor() == "" or dlg.GetDescriptor() == ".":
				# HACK: this should be the *real* name of the default object
				self.Descriptor = self.Media.Index["."][1].getAttribute("name")
			else:
				self.Descriptor = dlg.GetDescriptor()
			
			doc = self.Media.BuildObject(self.Descriptor)
			if doc:
				self.Controller.SetDocument(doc)
			else:
				self.OutputError("Unable to open specified Descriptor in file", "File open error...")
		dlg.Destroy()
	
	def OnFileSave(self, event):
		if self.Filename != "" and self.Descriptor != "":
			# write object back to media
			self.Media.WriteObject(self.Descriptor, self.Controller.Document)
			
			# clear command list
			self.Controller.ClearCommands()
			
			# write file
			self.Media.WriteFile(self.Filename)
		else:
			self.OnFileSaveAs(event)
	
	# TODO: This function works great for inseting animations into pre-existing 
	# files, but there are certain situations when we should really overwrite
	# files.  Find these situations and account for them.
	def OnFileSaveAs(self, event):
		from os.path import exists
		
		dlg = FileDescriptorDialog(self, -1, "Save animation...", self.Filename, ".", self.Descriptor, wxSAVE, self.Media)
		if dlg.ShowModal() == wxID_OK:
			# NOTE: why wouldn't this be valid?
			if dlg.GetDescriptor() == "" or dlg.GetDescriptor() == ".":
				self.OutputError("You must enter a valid animation name in the Descriptor field!", "Save Error")
				return
			
			filename = dlg.GetPath()
			descriptor = dlg.GetDescriptor()
			
			if exists(filename):
				# if a media object was created when the dialog was doing a Descriptor
				# lookup then we should try and reuse it.
				if dlg.Media and dlg.Media.Filename == filename:
					self.Media = dlg.Media
				else:
					self.Media.LoadFile(filename)
				
				if self.Filename != filename or self.Descriptor != descriptor:
					# this means that we are saving into a pre-existing file
					
					if self.Media.RootType == "animation2d":
						# this means we are overwritting an entire pre-existing
						# animation file.
						result = wxMessageBox("You are about to overwrite another media file.  Do you wish to continue?", "Query", wxYES_NO | wxICON_QUESTION, self)
						if result == wxNO:
							self.OutputError("Save operation cancelled", "Save Cancelled")
							return
						else:
							self.Media.CreateNew("animation2d")
					elif self.Media.Index.has_key(descriptor):
						# this means we are over writting an existing descriptor
						if self.Media.GetObjectType(descriptor) != "animation2d":
							# this means we are writting over a descriptor of a different type
							self.OutputError("Cannot overwrite a node of type " + self.Media.GetObjectType(descriptor), "Save Error")
							return
						else:
							# this means we are trying to overwrite an existing descriptor
							result = wxMessageBox("You are about to overwrite another Descriptor.  Do you wish to continue?", "Query", wxYES_NO | wxICON_QUESTION, self)
							if result == wxNO:
								self.OutputError("Save operation cancelled", "Save Cancelled")
								return
			else:
				self.Media.CreateNew("animation2d")
			
			# TODO: when we have the error handling ability, don't set these variables
			# until we have had a successful write
			self.Filename = dlg.GetPath()
			self.Descriptor = dlg.GetDescriptor()
			
			# write/clear/write
			self.Media.WriteObject(self.Descriptor, self.Controller.Document)
			self.Controller.ClearCommands()
			self.Media.WriteFile(self.Filename)
		
	def OnFileExit(self, event):
		if not self.QueryModified():
			return
		
		self.Close()
	
	def OnUndo(self, event):
		self.Controller.Undo()
	
	def OnRedo(self, event):
		self.Controller.Redo()
	
	def OnAddFrame(self, event):
		dlg = wxFileDialog(self, "Add Frame...", ".", "", "*.*", wxOPEN | wxMULTIPLE)
		if dlg.ShowModal() == wxID_OK:
			for path in dlg.GetPaths():
				if wxImage_CanRead(path):
					self.Controller.InsertFrame(self.Editor.Cursor, path)
				else:
					self.OutputError("Unable to open image file", "Add image error...")
		dlg.Destroy()
	
	def OnRemoveFrame(self, event):
		# do not remove if the cursor is in between frame.  Unintuitive!
		if not self.Editor.CursorMiddle:
			self.Controller.RemoveFrame(self.Editor.Cursor)

# 
# main:
#
if __name__ == "__main__":
	class EditorApp(wxApp):
		def OnInit(self):
			# load all the image handlers
			wxInitAllImageHandlers()
			
			# create the main window
			self.frame = AnimEditor(NULL, 1, wxDefaultPosition, wxSize(600, 400))
			self.frame.Show(True)
			self.SetTopWindow(self.frame)
			
			# set the anim editor window as the error handler
			import Media.base
			Media.base.error.SetHandlerObject(self.frame)
			
			return True
	
	app = EditorApp(0)
	app.MainLoop()



