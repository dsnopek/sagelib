# SAGElib Designer - The Graphical Game Design Platform
# Copyright 2002 David Snopek
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#

#
# Description: A set of media friendly dialogs
#

from wxPython.wx import *
from manager import Media

class NameDialog(wxDialog):
	def __init__(self, parent, title, name_label, default):
		wxDialog.__init__(self, parent, -1, title)
		
		main_sizer = wxFlexGridSizer(2, 2)
		main_sizer.Add(wxStaticText(self, -1, name_label), 0, wxALIGN_CENTER_VERTICAL | wxALIGN_RIGHT | wxALL, 5)
		self.NameEdit = wxTextCtrl(self, -1, default, wxDefaultPosition, wxSize(300, 25))
		main_sizer.Add(self.NameEdit, 0, wxALL, 5)
		
		main_sizer.Add(wxStaticText(self, -1, ""), 0, 0, 0) # spacer
		btn_sizer = wxBoxSizer(wxHORIZONTAL)
		btn_sizer.Add(wxButton(self, wxID_OK, "Ok", wxDefaultPosition, wxSize(100, 30)), 0, wxALL, 5)
		btn_sizer.Add(wxButton(self, wxID_CANCEL, "Cancel", wxDefaultPosition, wxSize(100, 30)), 0, wxALL, 5)
		main_sizer.Add(btn_sizer, 0, wxALIGN_LEFT | wxALL, 5)
		
		# this order is essential for wxMSW
		self.SetAutoLayout(True) # tell it that we want auto layout
		self.SetSizer(main_sizer) # set this sizer as our layout
		main_sizer.Fit(self) # resize dialog to fit
		main_sizer.SetSizeHints(self) # set to honor minimum size
		
	def GetName(self):
		return self.NameEdit.GetValue()

class BrowseDescriptorDialog(wxDialog):
	ID_LISTBOX = 101
	
	def __init__(self, parent, id, caption, Media):
		wxDialog.__init__(self, parent, id, caption)
		
		self.Media = Media
		top_sizer = wxBoxSizer(wxVERTICAL)
		
		self.ListBox = wxListBox(self, BrowseDescriptorDialog.ID_LISTBOX, wxDefaultPosition, wxSize(320, 200), [], wxLB_SINGLE)
		top_sizer.Add(self.ListBox, 0, wxALL, 5)
		
		btn_sizer = wxBoxSizer(wxHORIZONTAL)
		btn_sizer.Add(wxButton(self, wxID_OK, "Ok", wxDefaultPosition, wxSize(100, 30)), 0, wxALL | wxALIGN_CENTER, 5)
		btn_sizer.Add(wxButton(self, wxID_CANCEL, "Cancel", wxDefaultPosition, wxSize(100, 30)), 0, wxALL | wxALIGN_CENTER, 5)
		top_sizer.Add(btn_sizer, 0, wxALL | wxALIGN_CENTER, 0)
		
		# load values into the list box
		self.ReadIndex()
		
		# this order is essential for wxMSW
		self.SetAutoLayout(True) # tell it that we want auto layout
		self.SetSizer(top_sizer) # set this sizer as our layout
		top_sizer.Fit(self) # resize dialog to fit
		top_sizer.SetSizeHints(self) # set to honor minimum size
		
		EVT_LISTBOX_DCLICK(self, BrowseDescriptorDialog.ID_LISTBOX, self.OnDoubleClick)
		
	def OnDoubleClick(self, event):
		self.EndModal(wxID_OK)
	
	def ReadIndex(self):
		# make a Descriptor list
		descriptors = []
		for descriptor in self.Media.Index.items():
			# don't display the "." descriptor
			if descriptor[0] == ".":
				display_name = "<default> [%s]" % descriptor[1][0]
			else:
				display_name = "%s [%s]" % (descriptor[0], descriptor[1][0])
			
			descriptors.append((display_name, descriptor[0]))
		descriptors.sort()
		
		# put chioces and data into list box
		for descriptor in descriptors:
			self.ListBox.Append(descriptor[0], descriptor[1])
		
		# make selected by deafult
		self.ListBox.SetSelection(0)
	
	def GetDescriptor(self):
		return self.ListBox.GetClientData(self.ListBox.GetSelection())

class FileDescriptorDialog(wxDialog):
	ID_BROWSEFILE = 501
	ID_BROWSEDESCRIPTOR  = 502
	
	def __init__(self, parent, id, title, filename, path, descriptor, mode, media = None):
		wxDialog.__init__(self, parent, id, title)
		
		self.Media = media
		self.Path = path
		self.Mode = mode
		
		top_sizer = wxBoxSizer(wxVERTICAL)
		sizer = wxFlexGridSizer(2, 3, 2, 2)
		
		sizer.Add(wxStaticText(self, -1, "Filename:"), 0, wxALIGN_RIGHT | wxALIGN_CENTER_VERTICAL | wxTOP | wxLEFT, 10)
		self.FileEdit = wxTextCtrl(self, -1, filename, wxDefaultPosition, wxSize(300, 25))
		sizer.Add(self.FileEdit, 0, wxALIGN_CENTER_VERTICAL | wxTOP, 10)
		sizer.Add(wxButton(self, FileDescriptorDialog.ID_BROWSEFILE, "Browse..."), 0, wxTOP | wxRIGHT, 10)
		
		sizer.Add(wxStaticText(self, -1, "Descriptor:"), 0, wxALIGN_RIGHT | wxALIGN_CENTER_VERTICAL | wxLEFT | wxTOP, 5)
		self.DescriptorEdit = wxTextCtrl(self, -1, descriptor, wxDefaultPosition, wxSize(300, 25))
		sizer.Add(self.DescriptorEdit, 0, wxALIGN_CENTER_VERTICAL | wxTOP, 5)
		sizer.Add(wxButton(self, FileDescriptorDialog.ID_BROWSEDESCRIPTOR, "Browse..."), 0, wxTOP | wxRIGHT, 5)
		
		btn_sizer = wxBoxSizer(wxHORIZONTAL)
		btn_sizer.Add(wxButton(self, wxID_OK, "Ok", wxDefaultPosition, wxSize(100, 30)), 0, wxALL | wxALIGN_CENTER, 10)
		btn_sizer.Add(wxButton(self, wxID_CANCEL, "Cancel", wxDefaultPosition, wxSize(100, 30)), 0, wxALL | wxALIGN_CENTER, 10)
		
		top_sizer.Add(sizer, 0, wxALL, 0)
		top_sizer.Add(btn_sizer, 0, wxALIGN_CENTER, 0)
		
		# this order is essential for wxMSW
		self.SetAutoLayout(True) # tell it that we want auto layout
		self.SetSizer(top_sizer) # set this sizer as our layout
		top_sizer.Fit(self) # resize dialog to fit
		top_sizer.SetSizeHints(self) # set to honor minimum size
		
		EVT_BUTTON(self, FileDescriptorDialog.ID_BROWSEFILE, self.OnFileBrowse)
		EVT_BUTTON(self, FileDescriptorDialog.ID_BROWSEDESCRIPTOR, self.OnDescriptorBrowse)
		
	def OnFileBrowse(self, event):
		dlg = wxFileDialog(self, "Choose XML Media File", self.Path, self.FileEdit.GetValue(), "XML Media File (*.xml)|*.xml", self.Mode)
		if dlg.ShowModal() == wxID_OK:
			self.FileEdit.SetValue(dlg.GetPath())
		dlg.Destroy()
		
	def OnDescriptorBrowse(self, event):
		from os.path import exists
		
		if self.FileEdit.GetValue() == "":
			wxMessageBox("Must enter a file to read Descriptors from", "Descriptor Browse Error", wxOK | wxICON_EXCLAMATION, self)
			return
		
		if not exists(self.FileEdit.GetValue()):
			wxMessageBox("Cannot browse Descriptors in a non-existant file", "Descriptor Browse Error", wxOK | wxICON_EXCLAMATION, self)
			return
		
		# try to reuse the media object passed
		if not self.Media:
			self.Media = Media()
		
		if self.FileEdit.GetValue() != self.Media.Filename:
			self.Media.LoadFile(self.FileEdit.GetValue())
			# todo: add error checking
			#wxMessageBox("Unable to read Descriptors from file", "Error", wxOK | wxICON_EXCLAMATION, self)
		
		dlg = BrowseDescriptorDialog(self, -1, "Browse Descriptors (%s)" % self.FileEdit.GetValue(), self.Media)
		if dlg.ShowModal() == wxID_OK:
			self.DescriptorEdit.SetValue(dlg.GetDescriptor())
		dlg.Destroy()
		
	def GetPath(self):
		return self.FileEdit.GetValue()
		
	def GetDescriptor(self):
		return self.DescriptorEdit.GetValue()

