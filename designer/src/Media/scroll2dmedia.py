# SAGElib Designer - The Graphical Game Design Platform
# Copyright 2002 David Snopek
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#

""" Contains the media definition object for scroll-media """

from manager import MediaDefinition
import animation2d

Definition = MediaDefinition("http://sagelib.sourceforge.net/xml/dtd/scroll2d-media.dtd")
Definition.AddType("animation2d", animation2d.Document)

# set these nodes a trasparent
Definition.TransparentNodes = [ "scroll2d-media", "layer" ]

