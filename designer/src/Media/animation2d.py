# SAGElib Designer - The Graphical Game Design Platform
# Copyright 2002 David Snopek
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#

#
# Description: All the objects for dealing with a 2D animation
#

from wxPython.wx import *
import xml.dom.minidom, base

#
# Define the document.  Stores, loads and saves the data.
#

class Frame:
	def __init__(self, filename, dur = 0):
		self.Duration = dur
		self.LoadImage(filename)
		
	def LoadImage(self, filename):
		image = wxNullImage
		if image.LoadFile(filename):
			self.Bitmap = wxBitmapFromImage(image)
			self.Filename = filename
		else:
			self.Bitmap = wxNullBitmap
			self.Filename = ""

class Document:
	def __init__(self):
		self.FrameList = []
		
	def Read(self, root, doc, dirs):
		for node in root.childNodes:
			if node.nodeType == xml.dom.minidom.Node.ELEMENT_NODE:
				if node.tagName == "frame":
					dur = int(node.getAttribute("duration"))
					filename = node.getAttribute("image")
					
					path = dirs.GetPath(filename)
					if path:
						frame = Frame(path, dur)
						if frame.Bitmap != wxNullBitmap:
							self.FrameList.append(frame)
					else:
						base.error.Output("Cannot find bitmap image " + filename, "Animation document read error")
				else:
					# bad! throw exception?
					print "Unknown tag", node.tagName
	
	def BuildNode(self, doc, name):
		if name == "":
			base.error.Output("Cannot write an animation without a name!", "Animation document write error")
			return None
		
		root = doc.createElement("animation2d")
		root.setAttribute("name", name)
		
		# add frames
		for frame in self.FrameList:
			node = doc.createElement("frame")
			node.setAttribute("image", frame.Filename)
			node.setAttribute("duration", str(frame.Duration))
			root.appendChild(node)
		
		return root

#
# Performs operations on the document
#

class InsertFrameCommand:
	def __init__(self, document, index, filename, duration):
		self.Doc = document
		self.Index = index
		self.Filename = filename
		self.Dur = duration
	def Do(self):
		self.Doc.FrameList.insert(self.Index, Frame(self.Filename, self.Dur))
	def Undo(self):
		del self.Doc.FrameList[self.Index]

class RemoveFrameCommand:
	def __init__(self, document, index):
		self.Doc = document
		self.Index = index
	def Do(self):
		self.Frame = self.Doc.FrameList[self.Index]
		del self.Doc.FrameList[self.Index]
	def Undo(self):
		self.Doc.FrameList.insert(self.Frame)

class Controller(base.Controller):
	def __init__(self, doc = Document()):
		base.Controller.__init__(self, doc)
	def CreateNew(self):
		self.SetDocument(Document())
	def InsertFrame(self, index, filename, duration = 0):
		self.ExecCommand(InsertFrameCommand(self.Document, index, filename, duration))
	def RemoveFrame(self, index):
		self.ExecCommand(RemoveFrameCommand(self.Document, index))

#
# The wxWindows interface stuff
#

class EditorPanel(wxPanel):
	# constants
	BorderSize = 25
	HalfBorderSize = BorderSize / 2
	
	def __init__(self, parent, id, pos, size):
		wxPanel.__init__(self, parent, id, pos, size)
		
		self.Document = Document()
		self.Cursor = 0
		self.CursorMiddle = True
		self.FrameBackColor = wxWHITE
		self.AnimHeight = 0
		self.AnimTotalWidth = 0
		self.OffsetX = 0
		self.OffsetY = 0
		
		self.SetBackgroundColour(wxBLACK)
		
		# make the scroll bars
		self.SetWindowStyle(wxVSCROLL | wxHSCROLL)
		
		EVT_LEFT_UP(self, self.OnMouseUp)
		EVT_PAINT(self, self.Paint)
		EVT_SIZE(self, self.OnSize)
		EVT_SCROLLWIN(self, self.OnScroll)
	
	def SetDocument(self, document):
		self.Document = document
	
	# used to figure how long to draw cursors
	def CalcAnimSize(self):
		self.AnimHeight = 0
		self.AnimTotalWidth = EditorPanel.BorderSize
		for frame in self.Document.FrameList:
			# deal with hight
			if frame.Bitmap.GetHeight() > self.AnimHeight:
				self.AnimHeight = frame.Bitmap.GetHeight()
			
			# deal with total width
			self.AnimTotalWidth += frame.Bitmap.GetWidth() + EditorPanel.BorderSize
	
	def CalcScrollBars(self):
		width, height = self.GetClientSizeTuple()
		
		# test width
		if self.AnimTotalWidth > width:
			self.SetScrollbar(wxHORIZONTAL, self.OffsetX, width, self.AnimTotalWidth, True)
		else:
			self.SetScrollbar(wxHORIZONTAL, 0, 0, 0, True)
			self.OffsetX = 0
		
		# test height
		if self.AnimHeight + (EditorPanel.BorderSize * 2) > height:
			self.SetScrollbar(wxVERTICAL, self.OffsetY, height, self.AnimHeight + (EditorPanel.BorderSize * 2), True)
		else:
			self.SetScrollbar(wxVERTICAL, 0, 0, 0, True)
			self.OffsetY = 0
	
	def OnMouseUp(self, event):
		X = event.GetX() + self.OffsetX
		Y = event.GetY() + self.OffsetY
		
		# clip to reasonable Y
		if Y > self.AnimHeight + (EditorPanel.BorderSize * 2):
			return
		
		# get the clicked region
		frame_x = EditorPanel.BorderSize
		self.Cursor = 0
		for frame in self.Document.FrameList:
			if X < frame_x:
				self.CursorMiddle = True
				break;
			
			frame_x += frame.Bitmap.GetWidth()
			if X < frame_x:
				self.CursorMiddle = False
				break
			
			frame_x += EditorPanel.BorderSize
			self.Cursor += 1
		
		# repaint me
		self.Refresh()
	
	# mode:
	# 0 - normal frame
	# 1 - active selcected
	# 2 - background selected
	def DrawFrame(self, dc, frame, pos, mode):
		brush = wxBrush(self.FrameBackColor, wxSOLID)
		
		if mode == 0:
			pen = wxPen(wxBLUE, 2, wxSOLID)
		elif mode == 1:
			pen = wxPen(wxRED, 2, wxSOLID)
		elif mode == 2:
			pen = wxPen(wxGREEN, 2, wxSOLID)
			
		# draw border
		dc.SetPen(pen)
		dc.SetBrush(brush)
		dc.DrawRectangle(pos.x - 1, pos.y - 1, frame.Bitmap.GetWidth() + 3, frame.Bitmap.GetHeight() + 3)
		dc.SetPen(wxNullPen)
		dc.SetBrush(wxNullBrush)
		
		# draw image
		dc.DrawBitmap(frame.Bitmap, pos.x, pos.y, true)
	
	def DrawCursor(self, dc, x):
		dc.SetPen(wxRED_PEN)
		dc.DrawLine(x, EditorPanel.HalfBorderSize - self.OffsetY, x, EditorPanel.BorderSize + self.AnimHeight - self.OffsetY + EditorPanel.HalfBorderSize)
		dc.SetPen(wxNullPen)
		
	def Paint(self, event):
		dc = wxPaintDC(self)
		dc.BeginDrawing()
		
		# draw frames
		pos = wxPoint(EditorPanel.BorderSize - self.OffsetX, EditorPanel.BorderSize - self.OffsetY)
		for index in range(0, len(self.Document.FrameList)):
			frame = self.Document.FrameList[index]
			mode = 0
			if index == self.Cursor:
				if self.CursorMiddle is True:
					# draw mid frame cursor
					self.DrawCursor(dc, pos.x - EditorPanel.HalfBorderSize)
				else:
					mode = 1
			
			# draw actual frame
			self.DrawFrame(dc, frame, pos, mode)
			
			# prepare for next frame
			pos.x += frame.Bitmap.GetWidth() + EditorPanel.BorderSize
		
		# draw cursor at the end of the frame list
		if self.Cursor == len(self.Document.FrameList) and self.Cursor > 0:
			self.DrawCursor(dc, pos.x - EditorPanel.HalfBorderSize)
		
		dc.EndDrawing()

	def UpdateView(self):
		# the animation has just changed so update accordingly
		self.CalcAnimSize()
		self.CalcScrollBars()
		self.Refresh()
	
	def OnSize(self, event):
		self.CalcScrollBars()
	
	def OnScroll(self, event):
		pos = self.GetScrollPos(event.GetOrientation())
		if event.GetOrientation() == wxHORIZONTAL:
			self.OffsetX = pos
		else:
			self.OffsetY = pos
		self.Refresh()

