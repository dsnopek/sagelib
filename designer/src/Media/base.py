# SAGElib Designer - The Graphical Game Design Platform
# Copyright 2002 David Snopek
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#

class ErrorControl:
	def __init__(self):
		self.Handler = None
	def SetHandlerObject(self, obj):
		self.Handler = obj
	def Output(self, message, title):
		if self.Handler:
			self.Handler.OutputError(message, title)
		else:
			print "Error ->", title, ":", message

# make the control object
error = ErrorControl()

class Controller:
	"""
A class for organizing applications.  It implements a command structure with
full undo/redo, managing views and updating documents.  Using it is simple.
 (1) Write a document class. This should store the document data and deal with
     loading and saving it.
 (2) Write your command classes to perform operations on the document.  Each
     should provide a Do() and Undo() function and accept the document in the
	 constructor.
 (3) Extend the Controller class to provide hooks for creating and issuing the
     command objects.  Pass self.Document to the command objects to do their dirt.
 (4) Write a view class.  This must provide two functions: UpdateView() and 
     SetDocument(doc).  UpdateView is called when the document has been modified
	 and SetDocument(doc) is called when we change documents (note: the Controller
	 will call UpdateView() immediately after SetDocument(doc), so you don't have
	 to worry about it).
 (5) Write your application code.  You need to create your controller and any 
     views you need.  Be sure to call Controller.AddView(view) with your views.
	 Your controller class will be your interface to the design.  Once you have
	 created your document (possibly by loading from a file), call
	 Controller.SetDocument(doc) and let the editing begin!
	"""
	
	def __init__(self, document):
		self.Document = document
		self.ViewList = []
		self.ClearCommands()
	
	def SetDocument(self, doc):
		self.Document = doc
		self.ClearCommands()
		for view in self.ViewList:
			view.SetDocument(doc)
			view.UpdateView()
	
	def ExecCommand(self, command):
		# TODO: add some kind of command class validation (ie. callable(object.Do))
		
		# remove all items after the current, in the command list.  This happens
		# when you undo to a certain level then add new command, making it now
		# impossible to redo over the formerly undone, commands.
		while self.CommandIndex < len(self.CommandList):
			self.CommandList.pop()
		
		# add command
		self.CommandList.append(command)
		self.CommandIndex += 1
		
		# actually do
		command.Do()
		
		# mark as modified
		# TODO: If we execute a command then undo it, the document is really
		# still unmodified.  Update this to be more intelligent.
		self.Modified = True
		
		# update the views
		self.UpdateViews()
	
	def CanUndo(self):
		return self.CommandIndex > 0
	
	def Undo(self):
		if self.CommandIndex > 0:
			self.CommandIndex -= 1
			self.CommandList[self.CommandIndex].Undo()
			self.UpdateViews()
	
	def CanRedo(self):
		return self.CommandIndex < len(self.CommandList)
	
	def Redo(self):
		if self.CommandIndex < len(self.CommandList):
			self.CommandList[self.CommandIndex].Do()
			self.CommandIndex += 1
			self.UpdateViews()
	
	def ClearCommands(self):
		self.CommandList = []
		self.CommandIndex = 0
		self.Modified = False
		
	def UpdateViews(self):
		for view in self.ViewList:
			view.UpdateView()
	
	def AddView(self, view):
		self.ViewList.append(view)
		view.SetDocument(self.Document)
		view.UpdateView()


