
# A makefile target for uploading xml dtd files.  You must set the DTD_LIST
# macro to the files to upload.  It depends on SSH and having access to write 
# this information.  If your SF.net user name isn't the same as your local
# user name, you'll have to set the "SF_USERNAME" variable, for example:
#
# make SF_USERNAME=jdog upload-dtd
#

SF_USERNAME=
DTD_DEST=sagelib.sourceforge.net:/home/groups/s/sa/sagelib/htdocs/xml/dtd/

upload-dtd:
	if test x$(DTD_LIST) = x ; then \
		echo *** ERROR: THIS MAKEFILE IS IMPROPERLY CONSTRUCTED ***; \
		echo *** PLEASE CONTACT THE MAINTAINER OF THESE DOCS!!  ***; \
		return 1; \
	fi; \
	if test x$(SF_USERNAME) != x ; then \
		scp $(DTD_LIST) $(SF_USERNAME)@$(DTD_DEST); \
	else \
		scp $(DTD_LIST) $(DTD_DEST); \
	fi;


