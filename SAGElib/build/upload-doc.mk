
# A makefile target for uploading the html documentation to the SourceForge
# based website.  It depends on SSH and having access to write this 
# information.  If your SF.net user name isn't the same as your local
# user name, you'll have to set the "SF_USERNAME" variable, for example:
#
# make SF_USERNAME=jdog upload
#

SF_USERNAME=
DOC_DEST=libksd.sourceforge.net:/home/groups/l/li/libksd/htdocs/doc/@KSD_VERSION@/$(DOC_NAME)/

upload:all
	if test x$(DOC_NAME) = x ; then \
		echo *** ERROR: THIS MAKEFILE IS IMPROPERLY CONSTRUCTED ***; \
		echo *** PLEASE CONTACT THE MAINTAINER OF THESE DOCS!!  ***; \
		return 1; \
	fi; \
	if test x$(SF_USERNAME) != x ; then \
		scp html/* $(SF_USERNAME)@$(DOC_DEST); \
	else \
		scp html/* $(DOC_DEST); \
	fi;


