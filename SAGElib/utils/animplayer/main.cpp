
#include <iostream>
#include <ksd/Application.h>
#include <ksd/Plugin.h>
#include "2d/Animation2D.h"
#include "2d/AnimatedObject.h"
#include "xml/XMLParser.h"

// a hack! Cannot use in production code!
#include "drivers/scroll2d/Scroll2DDriver.h"

using namespace ksd;
using namespace std;

class TAnimationPlayer : public TApplication {
public:
	TAnimationPlayer() {
		Animation = NULL;
		ScreenObject = NULL;
	}

	void Init() {
		std::string filename, anim_name;

		// load imglib plugins
		#ifndef KSD_NO_PLUGINS
		LoadPlugin("libimage");
		#endif
		
		// parse the arguments
		if(GetArgCount() >= 2) {
			filename = GetArgList()[1];

			if(GetArgCount() == 3) {
				anim_name = GetArgList()[2];
			}
			if(GetArgCount() > 3) {
				cerr << "Too many arguments!" << endl;
				Quit(1);
			}
		} else {
			cerr << "Not enough arguments!" << endl;
			Quit(1);
		}

		// load the animation
		LoadAnimation(filename, anim_name);

		// create screen
		try {
			CreateScreen(Animation->GetWidth(), Animation->GetHeight());
		} catch(ksd::exception& e) {
			cerr << "ERROR: Unable to display animation!" << endl;
			e.report();
			Quit(1);
		}

		// set animation monitor
		AnimMonitor.SetAnimation(Animation);
		AnimMonitor.Start();
	}

	void Shutdown() {
		if(ScreenObject) {
			delete ScreenObject;
		} 
		// Do not delete the animation if we have a screen object since the animation
		// resides within the screen object
		else {
			if(Animation) {
				delete Animation;
			}
		}
	}

	bool Draw(TCanvas* Canvas, DrawType dt);
private:
	SAGE::Driver2D::TAnimation2D* Animation;
	SAGE::Driver2D::TAnimatedObject* ScreenObject;
	SAGE::Driver2D::TAnimationMonitor2D AnimMonitor;
	SAGE::TDisplayDriver* DisplayDriver;

	std::string GetDocumentType(const std::string& filename);
	void LoadAnimation(const std::string& filename, const std::string& descriptor);
};

KSD_MAIN(TAnimationPlayer);

std::string TAnimationPlayer::GetDocumentType(const std::string& filename)
{
	SAGE::TXMLParser parser;	

	// load it
	parser.Load(filename);

	// return the name of the root node
	return parser.GetRoot().get_name();
}

void TAnimationPlayer::LoadAnimation(const std::string& filename, const std::string& descriptor)
{
	std::string doc_type;

	// load document type
	doc_type = GetDocumentType(filename);

	if(doc_type == "animation2d") {
		if(descriptor != "") {
			cerr << "ERROR: Cannot specify a descriptor with an animation file!" << endl;
			Quit(1);
		}

		// Create an animation
		Animation = new SAGE::Driver2D::TAnimation2D;

		// load a pure animation file
		Animation->Load(filename);

		// set the caption
		SetCaption(Animation->GetName());
	} else if(doc_type == "screen-object") {
		if(descriptor == "") {
			cerr << "ERROR: Must specify an animation to extract from the screen-object" << endl;
			Quit(1);
		}

		// create a screen object
		ScreenObject = new SAGE::Driver2D::TAnimatedObject;

		// parse file
		ScreenObject->Load(filename);

		// copy animation out of screen object
		Animation = ScreenObject->GetAnimation(descriptor);
		if(!Animation) {
			cerr << "ERROR: Cannot find animation named \"" << descriptor
				 << "\" inside of screen-object named \"" << ScreenObject->GetName()
				 << "\"" << endl;
			Quit(1);
		}

		// set the name
		SetCaption(ScreenObject->GetName() + "::" + Animation->GetName());
	} else if(doc_type == "scroll2d-media") {
		// TODO: we won't be able to include handlers for all media types, because 
		// they can be written as plugins.  Provide a mechanism that allows the user
		// to load the driver necessary and try to use that to load a media file

		SAGE::Driver2D::IMedia2D* media;
		std::string object_name, anim_name;
		unsigned int index;

		if(descriptor == "") {
			cerr << "ERROR: Must specify an character::animation descriptior to extract from the media file" << endl;
			Quit(1);
		}

		// NOTE: a hack! Cannot use in prodution code!
		// allocate new scroll 2d driver
		DisplayDriver = new SAGE::TScroll2DDriver;

		// load media
		DisplayDriver->LoadMediaDefinition(filename);

		// get media interface
		media = DisplayDriver->GetMedia2DInterface();
		if(!media) {
			cerr << "ERROR: This isn't a 2D driver!" << endl;
			Quit(1);
		}

		// get object/animation name
		index = descriptor.find("::");
		if(index >= descriptor.length()) {
			cerr << "ERROR: Invalid descriptor, \"" << descriptor << "\"!" << endl;
			Quit(1);
		}
		object_name = descriptor.substr(0, index);
		anim_name = descriptor.substr(index + 2, descriptor.length() - index - 2);

		// get object and animation
		ScreenObject = media->GetAnimatedObject(object_name);
		if(!ScreenObject) {
			cerr << "ERROR: Cannot find screen object named \"" 
				 << object_name << "\" in file \""
				 << filename << "\"" << endl;
			Quit(1);
		}
		Animation = ScreenObject->GetAnimation(anim_name);
		if(!Animation) {
			cerr << "ERROR: Cannot find animation named \""
				 << anim_name << "\" inside of screen-object named \""
				 << object_name << "\"" << endl;
			Quit(1);
		}

		// deallocate media interface object
		delete media;

		// set the window name
		SetCaption(descriptor);
	} else {
		cerr << "ERROR: The file \"" << filename 
			 << "\" has unknown document type: \"" 
			 << doc_type << "\"" << endl;
		Quit(1);
	}
}

bool TAnimationPlayer::Draw(TCanvas* Canvas, DrawType dt)
{
	if(AnimMonitor.Update() || dt == REPAINT) {
		AnimMonitor.Draw(Canvas, SAGE::TScreenPosition(0, 0));
		return true;
	}
	
	return false;
}

