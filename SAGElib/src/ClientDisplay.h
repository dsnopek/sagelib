//--------------------------------------------------------------------------- 
//	SAGElib -- Scene-Actor Game Engine
//	Copyright 2001 David Snopek
//	
//	This library is free software; you can redistribute it and/or modify
//	it under the terms of the GNU General Public License as published by
//	the Free Software Foundation; either version 2 of the License, or
//	(at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//
//	You should have received a copy of the GNU General Public License
//	along with this program; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
//--------------------------------------------------------------------------- 
//	File:			$RCSfile$
//	
//	Author:			David Snopek
//	Revised by:		
//	Version:		$Revision$
//	
//	Objects:		TClient
//	
//	Purpose:		A libksd widget that outputs the game data to the screen 
//					and sends input back to the game server.
//	
//	Checked-in by:	$Author$
//					$Date$
//---------------------------------------------------------------------------
#ifndef _SAGE_ClientDisplayH_
#define _SAGE_ClientDisplayH_
//---------------------------------------------------------------------------
#include <ksd/CustomWidget.h>
//--------------------------------------------------------------------------- 
#ifdef __GNUG__
#pragma interface
#endif
//---------------------------------------------------------------------------
namespace SAGE {
//---------------------------------------------------------------------------
/** A libksd widget that outputs the game data to the screen and sends input
 ** back to the game server.  A client does not imply a single player; it is
 ** an entire environment.  In most games the client and server will be on
 ** the same machine and run from the same process.
 ** 
 ** The client must be given an output "driver" and media files that the driver
 ** can read, inorder to display game output.  It also must be given instructions
 ** on how to interpret the player input to send information back to the server.
 */
class TClientDisplay : ksd::TCustomWidget {
public:
	TClientDisplay(ksd::TWidget* _parent) : ksd::TCustomWidget(_parent) { }
	~TClientDisplay() { };

private:
	bool Draw(ksd::TCanvas* Canvas, DrawType dt);
};
//---------------------------------------------------------------------------  
}; // namespace SAGE
//---------------------------------------------------------------------------  
#endif

