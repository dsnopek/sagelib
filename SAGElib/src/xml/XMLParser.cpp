//--------------------------------------------------------------------------- 
//	SAGElib -- Scene-Actor Game Engine
//	Copyright 2001 David Snopek
//	
//	This library is free software; you can redistribute it and/or modify
//	it under the terms of the GNU General Public License as published by
//	the Free Software Foundation; either version 2 of the License, or
//	(at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//
//	You should have received a copy of the GNU General Public License
//	along with this program; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//		
//--------------------------------------------------------------------------- 
//	$Header$
//	
//	Author:			David Snopek
//	Revised by:		
//	Version:		$Revision$
//	
//	Checked-in by:	$Author$
//	
//--------------------------------------------------------------------------- 
#ifdef __GNUG__
#pragma implementation
#endif
//---------------------------------------------------------------------------
#define BUILD_LIB
#include <iostream>
#include <string.h>
#include <libgen.h>
#include "XMLParser.h"

using namespace SAGE;
using namespace std;
//---------------------------------------------------------------------------  
static bool Initialized = false;
//---------------------------------------------------------------------------  
// a dumb hack to clean up the parser at shutdown
class _init_object {
public:
	~_init_object() {
		if(Initialized) xmlCleanupParser();
	}
};
static _init_object _io;
//---------------------------------------------------------------------------  
std::string SAGE::GetXMLDocumentType(const std::string& filename)
{
	TXMLParser parser;
	std::string type;

	parser.Load(filename);
	if(!parser.GetRoot().is_valid()) {
		cerr << "Can't detect document type!" << endl;
		return "";
	}

	return parser.GetRoot().get_name();
}
//---------------------------------------------------------------------------
TXMLParser::TXMLParser()
{
	doc = NULL;

	if(!Initialized) {
		xmlInitParser();
	}
}
//---------------------------------------------------------------------------  
TXMLParser::~TXMLParser()
{
	if(doc) {
		xmlFreeDoc(doc);
	}	
}
//---------------------------------------------------------------------------  
void TXMLParser::Load(const std::string& _fn) throw(EXMLParseError)
{
	if(doc) {
		xmlFreeDoc(doc);
	}

	Filename = _fn;
	doc = xmlParseFile(Filename.c_str());

	if(!doc) {
		throw EXMLParseError(std::string("Unable to correctly parse ") + Filename);
	}

	// get the current directory
	MakeCurDir();
}
//---------------------------------------------------------------------------  
void TXMLParser::MakeCurDir()
{
	unsigned int pos;
	char* base;

	// get basename
	//base = basename(Filename.c_str());

	// find pos
	//pos = Filename.find(std::string(base));

	// cut out other part
	//CurrentDirectory = Filename.substr(0, pos);
	CurrentDirectory = dirname((char*)Filename.c_str());
}
//---------------------------------------------------------------------------  

