//--------------------------------------------------------------------------- 
//	SAGElib -- Scene-Actor Game Engine
//	Copyright 2001 David Snopek
//	
//	This library is free software; you can redistribute it and/or modify
//	it under the terms of the GNU General Public License as published by
//	the Free Software Foundation; either version 2 of the License, or
//	(at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//
//	You should have received a copy of the GNU General Public License
//	along with this program; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
//--------------------------------------------------------------------------- 
//	File:			$RCSfile$
//	
//	Author:			David Snopek
//	Revised by:		
//	Version:		$Revision$
//	
//	Objects:		TXMLIterator
//	
//	Purpose:		C++-ify libxml2
//	
//	Checked-in by:	$Author$
//					$Date$
//---------------------------------------------------------------------------
#ifndef _SAGE_XMLIteratorH_
#define _SAGE_XMLIteratorH_
//---------------------------------------------------------------------------
#include <string>
#include <libxml/xmlmemory.h>
#include <libxml/parser.h>
#include <libxml/tree.h>
#include <ksd/exception.h>
//---------------------------------------------------------------------------
namespace SAGE {

// forward decl
class TXMLParser;
//---------------------------------------------------------------------------  
class EXMLUnknownAttribute : public ksd::exception {
public:
	EXMLUnknownAttribute(const std::string& attrib) 
		: ksd::exception(attrib, "XMLParser") { }
};
//---------------------------------------------------------------------------
// TODO: Add XML namespaces to this equation.
/** An iterator class, that can be used to traverse an XML DOM tree.
 */
class TXMLIterator {
public:
	TXMLIterator() {
		Parser = NULL;
		Internal = NULL;
	}

	TXMLIterator(const TXMLIterator& V) {
		Parser = V.Parser;
		Internal = V.Internal;
	}

	explicit TXMLIterator(TXMLParser* _parser, xmlNodePtr _ptr) {
		Parser = _parser;
		Internal = _ptr;
	}

	/// Steps to the next element
	void next() { Internal = Internal->next; }
	/// Steps to the first child
	void first_child() { Internal = Internal->xmlChildrenNode; }

	TXMLIterator& operator ++ () { next(); return *this; }
	TXMLIterator operator ++ (int) {
		TXMLIterator temp = *this;
		next();
		return temp;
	}

	/** Returns true if this is a valid iterator.  The iterator will be invalidated
	 ** when you run out of elements.  Check for this in your loops.
	 */
	bool is_valid() const { return Internal != NULL; }

	/** Returns true if this is a blank node.
	 */
	bool is_blank() const { return xmlIsBlankNode(Internal); }

	/** Returns true if the node has this attribute.
	 */
	bool has_attribute(const std::string& attrib) {
		return xmlHasProp(Internal, (const xmlChar*)attrib.c_str()) != NULL;
	}

	/// Returns true if the name of this node is the test string
	bool name_equals(const std::string& test) const {
		return xmlStrcmp(Internal->name, (xmlChar*)test.c_str()) == 0;
	}

	/// Returns the parser to which this iterator is attached
	TXMLParser* get_parser() { return Parser; }
	/// Returns the name of the tag
	std::string get_name() const { return std::string((char*)Internal->name); }
	/// Returns the value of an attribute to the tag
	std::string get_attribute(const std::string& attrib) const throw(EXMLUnknownAttribute) { 
		xmlChar* temp = xmlGetProp(Internal, (const xmlChar*)attrib.c_str());
		if(temp) return std::string((char*)temp);
		else {
			throw EXMLUnknownAttribute(attrib);
		}
	}

	/// Returns a string of all the data in the tag
	std::string get_string() const;
private:
	TXMLParser* Parser;
	xmlNodePtr Internal;
};
//---------------------------------------------------------------------------  
}; // namespace SAGE
//---------------------------------------------------------------------------  
#endif

