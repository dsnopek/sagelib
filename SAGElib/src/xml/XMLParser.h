//--------------------------------------------------------------------------- 
//	SAGElib -- Scene-Actor Game Engine
//	Copyright 2001 David Snopek
//	
//	This library is free software; you can redistribute it and/or modify
//	it under the terms of the GNU General Public License as published by
//	the Free Software Foundation; either version 2 of the License, or
//	(at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//
//	You should have received a copy of the GNU General Public License
//	along with this program; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
//--------------------------------------------------------------------------- 
//	File:			$RCSfile$
//	
//	Author:			David Snopek
//	Revised by:		
//	Version:		$Revision$
//	
//	Objects:		TXMLParser
//	
//	Purpose:		An xml parser class.  For internal use only.
//	
//	Checked-in by:	$Author$
//					$Date$
//---------------------------------------------------------------------------
#ifndef _SAGE_XMLParserH_
#define _SAGE_XMLParserH_
//---------------------------------------------------------------------------
#include <libxml/parser.h>
#include <ksd/exception.h>
#include "XMLIterator.h"
//--------------------------------------------------------------------------- 
#ifdef __GNUG__
#pragma interface
#endif
//---------------------------------------------------------------------------
namespace SAGE {

// forward decl
class XMLIterator;
//---------------------------------------------------------------------------  
class EXMLParseError : public ksd::exception {
public:
	EXMLParseError(const std::string& msg) : ksd::exception("XMLParser", msg) { }
};
//---------------------------------------------------------------------------  
/** Returns the name of the root tag.  This is useful for doing quick checks 
 ** on xml document types.
 */
std::string GetXMLDocumentType(const std::string& filename);
//---------------------------------------------------------------------------
/** An xml parser class.  TXMLParser objects aren't intended to have very long
 ** lifetimes.  Create it and extract the required data then destroy it.
 */
class TXMLParser { friend class TXMLIterator;
public:
	TXMLParser();
	~TXMLParser();

	/// Parse an xml file.
	void Load(const std::string& filename) throw(EXMLParseError);

	/// Returns an iterator to the root node
	TXMLIterator GetRoot() const { 
		return TXMLIterator((TXMLParser*)this, xmlDocGetRootElement(doc)); 
	}

	/// Returns the directory that the loaded XML file is in
	std::string GetCurrentDirectory() const {
		return CurrentDirectory;
	}
	
	/// Returns the filename that we are parsing
	std::string GetFilename() const {
		return Filename;
	}
private:
	std::string Filename, CurrentDirectory;
	xmlDocPtr doc;

	void MakeCurDir();
};
//---------------------------------------------------------------------------  
}; // namespace SAGE
//---------------------------------------------------------------------------  
#endif

