//--------------------------------------------------------------------------- 
//	SAGElib -- Scene-Actor Game Engine
//	Copyright 2001 David Snopek
//	
//	This library is free software; you can redistribute it and/or modify
//	it under the terms of the GNU General Public License as published by
//	the Free Software Foundation; either version 2 of the License, or
//	(at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//
//	You should have received a copy of the GNU General Public License
//	along with this program; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
//--------------------------------------------------------------------------- 
//	File:			$RCSfile$
//	
//	Author:			David Snopek
//	Revised by:		
//	Version:		$Revision$
//	
//	Objects:		TAnimation2D
//	
//	Purpose:		@DESCRIPTION@
//	
//	Checked-in by:	$Author$
//					$Date$
//---------------------------------------------------------------------------
#ifndef _SAGE_Animation2DH_
#define _SAGE_Animation2DH_
//---------------------------------------------------------------------------
#include <string>
#include <vector>
#include <ksd/Canvas.h>
#include <ksd/Timer.h>
#include "types.h"
//--------------------------------------------------------------------------- 
#ifdef __GNUG__
#pragma interface
#endif
//---------------------------------------------------------------------------
namespace SAGE {

// forward decl
class TXMLIterator;

namespace Driver2D {
//---------------------------------------------------------------------------
class TAnimation2D {
public:
	TAnimation2D();
	TAnimation2D(const std::string& _name);
	~TAnimation2D();

	/// Get the animation name
	std::string GetName() const { return Name; }
	/// Get the animation width
	unsigned int GetWidth() const { return Width; }
	/// Get the animation height
	unsigned int GetHeight() const { return Height; }
	/// Get the number of frames
	unsigned int GetFrameCount() const { return FrameList.size(); }

	/// Returns true if this is a valid animation
	bool IsValid() const { return Name != "" && GetFrameCount() > 0; }

	/// Clears animation.
	void Clear();

	/** Load an xml animation definition file.
	 */
	void Load(const std::string& filename);

	/** Load an animation from a peice of xml.
	 */
	void Load(const TXMLIterator& xml_iter);

	/** Copies another animation.
	 */
	void CopyFrom(const TAnimation2D& animation);

	/** Draw a frame.
	 */
	void DrawFrame(ksd::TCanvas* Canvas, unsigned int frame, TScreenPosition pos);

	/** Gets the duration of a frame in milliseconds.
	 */
	int GetDuration(unsigned int frame) { return FrameList[frame].Duration; }
private:
	struct Frame {
		ksd::TImage* Image;
		unsigned int Duration;
	};

	std::string Name;
	std::vector<Frame> FrameList;
	unsigned int Width, Height;
};
//---------------------------------------------------------------------------  
class TAnimationMonitor2D {
public:
	TAnimationMonitor2D() {
		Animation = NULL;
		Reset();
	}

	/** Sets the animation to monitor.
	 */
	void SetAnimation(TAnimation2D* _anim) {
		Animation = _anim;
		Reset();
	}

	/** Returns the animation used.
	 */
	TAnimation2D* GetAnimation() const { return Animation; }

	/** Returns true if we have a valid animation to play.
	 */
	bool IsValid() const { return Animation && Animation->IsValid(); }

	/** Resets the animation state, stopping the animation if necessary.
	 */
	void Reset() {
		Running = false;
		Frame = 0;
		Time = 0;
	}

	/** Call this to update the frame state.  Returns true if the frame state
	 ** was in fact updated.
	 */
	bool Update();

	/** Updates the frame state then draws the specified frame.
	 */
	void Draw(ksd::TCanvas* Canvas, TScreenPosition pos);

	/** Pause the counting of the frame duration.
	 */
	void Stop();

	/** Start the timer again if it had been stopped.
	 */
	void Start();
private:
	TAnimation2D* Animation;
	unsigned int Frame, Time;
	ksd::TDeltaTimer Timer;
	bool Running;
};
//---------------------------------------------------------------------------  
}; // namespace Driver2D
}; // namespace SAGE
//---------------------------------------------------------------------------  
#endif

