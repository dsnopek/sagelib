//--------------------------------------------------------------------------- 
//	SAGElib -- Scene-Actor Game Engine
//	Copyright 2001 David Snopek
//	
//	This library is free software; you can redistribute it and/or modify
//	it under the terms of the GNU General Public License as published by
//	the Free Software Foundation; either version 2 of the License, or
//	(at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//
//	You should have received a copy of the GNU General Public License
//	along with this program; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//		
//--------------------------------------------------------------------------- 
//	$Header$
//	
//	Author:			David Snopek
//	Revised by:		
//	Version:		$Revision$
//	
//	Checked-in by:	$Author$
//	
//--------------------------------------------------------------------------- 
#ifdef __GNUG__
#pragma implementation
#endif
//---------------------------------------------------------------------------
#define BUILD_LIB
#include <iostream>
#include "XMLParser.h"
#include "AnimatedObject.h"

using namespace std;
using namespace SAGE;
using namespace SAGE::Driver2D;
//---------------------------------------------------------------------------
TAnimatedObject::TAnimatedObject()
{

}
//---------------------------------------------------------------------------  
TAnimatedObject::~TAnimatedObject()
{

}
//---------------------------------------------------------------------------  
void TAnimatedObject::Clear()
{
	list_type::iterator i;
	for(i = AnimationList.begin(); i != AnimationList.end(); i++) {
		delete (*i).second;
	}
	AnimationList.clear();
}
//---------------------------------------------------------------------------  
void TAnimatedObject::Load(const std::string& filename)
{
	TXMLParser parser;

	// do actual loading
	parser.Load(filename);
	Load(parser.GetRoot());
}
//---------------------------------------------------------------------------  
void TAnimatedObject::Load(const TXMLIterator& root_iter)
{
	TAnimation2D* Animation;
	TXMLIterator xml_iter;
	std::string cur_dir;

	if(!root_iter.is_valid()) {
		cerr << "Passed an invalid xml iterator to TAnimatedObject!" << endl;
		return;
	}

	// NOTE: We don't care what name this tag is, just so long as the rest of the
	// data we need is provided!

	// get name
	try {
		SetName(root_iter.get_attribute("name"));
	} catch(EXMLUnknownAttribute& e) {
		cerr << "Animated screen object has no name attribute" << endl;
		return;
	}

	// clear old object
	Clear();
	
	// point to first child	
	xml_iter = root_iter;
	xml_iter.first_child();

	// make cur dir
	cur_dir = xml_iter.get_parser()->GetCurrentDirectory();

	// get animations
	while(xml_iter.is_valid()) {
		// ignore blanks
		if(!xml_iter.is_blank()) {
			// make new animation object
			Animation = new TAnimation2D;

			// load either inlined or included animations
			if(xml_iter.name_equals("animation2d")) {
				Animation->Load(xml_iter);
			} else if(xml_iter.name_equals("include")) {
				std::string include_filename;
				try {
					include_filename = xml_iter.get_attribute("filename");

					// load animation file
					Animation->Load(cur_dir + include_filename);
				} catch(EXMLUnknownAttribute& e) {
					cerr << "Include tag has no filename!" << endl;
				}
			}

			// check for valid animation
			if(Animation->IsValid()) {
				AddAnimation(Animation);
			} else {
				delete Animation;
			}
		}

		// increment
		++xml_iter;	
	}
}
//---------------------------------------------------------------------------  

