//--------------------------------------------------------------------------- 
//	SAGElib -- Scene-Actor Game Engine
//	Copyright 2001 David Snopek
//	
//	This library is free software; you can redistribute it and/or modify
//	it under the terms of the GNU General Public License as published by
//	the Free Software Foundation; either version 2 of the License, or
//	(at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//
//	You should have received a copy of the GNU General Public License
//	along with this program; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
//--------------------------------------------------------------------------- 
//	File:			$RCSfile$
//	
//	Author:			David Snopek
//	Revised by:		
//	Version:		$Revision$
//	
//	Objects:		TAnimatedObject
//	
//	Purpose:		@DESCRIPTION@
//	
//	Checked-in by:	$Author$
//					$Date$
//---------------------------------------------------------------------------
#ifndef _SAGE_AnimatedObjectH_
#define _SAGE_AnimatedObjectH_
//---------------------------------------------------------------------------
#include <map>
#include <sigc++/signal_system.h>
#include "ScreenObject.h"
#include "Animation2D.h"
//--------------------------------------------------------------------------- 
#ifdef __GNUG__
#pragma interface
#endif
//---------------------------------------------------------------------------
namespace SAGE {
namespace Driver2D {
//---------------------------------------------------------------------------
// TODO: seperate TAnimatedObject into TAnimatedObjectMedia so that one media 
// object can be used across several TAnimatedObject as well as transparent 
// single use.
class TAnimatedObject : public TScreenObject, public SigC::Object {
public:
	TAnimatedObject();
	~TAnimatedObject();

	void Clear();

	void Load(const std::string& filename);
	void Load(const TXMLIterator& xml_iter);	
	
	void Draw(ksd::TCanvas*	Canvas) {
		if(AnimMonitor.IsValid() && AnimMonitor.Update()) {
			///AnimMonitor.Draw(...);
		}
	}

	void ShowAction(const std::string& action) {
		AnimMonitor.SetAnimation(GetAnimation(action));
	}

	void AddAnimation(TAnimation2D* Animation) {
		AnimationList.insert(std::make_pair(Animation->GetName(), Animation));
	}

	TAnimation2D* GetAnimation(const std::string& _name) {
		list_type::iterator i = AnimationList.find(_name);
		if(i != AnimationList.end()) {
			return (*i).second;
		} 
		
		return NULL;
	}
private:
	typedef std::map<std::string, TAnimation2D*> list_type;
	list_type AnimationList;
	
	TAnimationMonitor2D AnimMonitor;
};
//---------------------------------------------------------------------------  
}; // namespace Driver2D
}; // namespace SAGE
//---------------------------------------------------------------------------  
#endif

