//--------------------------------------------------------------------------- 
//	SAGElib -- Scene-Actor Game Engine
//	Copyright 2001 David Snopek
//	
//	This library is free software; you can redistribute it and/or modify
//	it under the terms of the GNU General Public License as published by
//	the Free Software Foundation; either version 2 of the License, or
//	(at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//
//	You should have received a copy of the GNU General Public License
//	along with this program; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//		
//--------------------------------------------------------------------------- 
//	$Header$
//	
//	Author:			David Snopek
//	Revised by:		
//	Version:		$Revision$
//	
//	Checked-in by:	$Author$
//	
//--------------------------------------------------------------------------- 
#ifdef __GNUG__
#pragma implementation
#endif
//---------------------------------------------------------------------------
#define BUILD_LIB
#include <iostream>
#include <ksd/Image.h>
#include "Animation2D.h"
#include "XMLParser.h"

using namespace std;
using namespace SAGE;
using namespace SAGE::Driver2D;
//---------------------------------------------------------------------------
TAnimation2D::TAnimation2D(const std::string& _name)
{
	Name = _name;
	Width = Height = 0;
}
//---------------------------------------------------------------------------  
TAnimation2D::TAnimation2D()
{
	Width = Height = 0;
}
//---------------------------------------------------------------------------  
TAnimation2D::~TAnimation2D()
{
	Clear();
}
//---------------------------------------------------------------------------  
void TAnimation2D::Clear()
{
	// clean up frames
	while(FrameList.size() > 0) {
		if(FrameList.back().Image) 
			delete FrameList.back().Image;
		FrameList.pop_back();
	}

	Width = Height = 0;
}
//---------------------------------------------------------------------------  
void TAnimation2D::Load(const std::string& filename)
{
	TXMLParser parser;

	// load
	parser.Load(filename);
	Load(parser.GetRoot());
}
//---------------------------------------------------------------------------  
void TAnimation2D::Load(const TXMLIterator& root_node) 
{
	TXMLIterator xml_iter;
	std::string cur_dir, filename;

	if(!root_node.is_valid()) {
		cerr << "Passed an invalid xml iterator to TAnimation2D!" << endl;
		return;
	}	

	// check for proper tag type
	if(!root_node.name_equals("animation2d")) {
		cerr << "Expected animation2d tag but got " << root_node.get_name() << endl;
		return;
	}

	// clear the old animation
	Clear();

	// get the animation name
	try {
		Name = root_node.get_attribute("name");
	} catch(EXMLUnknownAttribute& e) {
		cerr << "animation2d has no name!" << endl;
		return;
	}

	// point to first child
	xml_iter = root_node;
	xml_iter.first_child();

	// make cur dir and filename
	cur_dir = xml_iter.get_parser()->GetCurrentDirectory();
	filename = xml_iter.get_parser()->GetFilename();

	// a macro to output specific information about the location of this animation
	// for use in error messages.
	#define OUTPUT_ME(stream) \
		stream << "In animation \"" << Name << "\" from " << filename << ": "
		
	// get the frames
	while(xml_iter.is_valid()) {
		// ignore blanks
		if(!xml_iter.is_blank()) {
			// animation2d can only have frame tags
			if(!xml_iter.name_equals("frame")) {
				OUTPUT_ME(cerr);
				cerr << "Expected frame tag but got " << xml_iter.get_name() << endl;
				// TODO: throw exception
			} else { // make a frame
				Frame new_frame;
				std::string image_name;

				// get duration
				try {
					std::string temp = xml_iter.get_attribute("duration");
					new_frame.Duration = atoi(temp.c_str());
				} catch(EXMLUnknownAttribute& e) {
					OUTPUT_ME(cerr);
					cerr << "Frame tag has no duration!" << endl;
					new_frame.Duration = 0;
				}

				// load image
				try {
					image_name = xml_iter.get_attribute("image");
					new_frame.Image = new ksd::TImage;
					new_frame.Image->Load((cur_dir + image_name).c_str());
				} catch(EXMLUnknownAttribute& e) {
					OUTPUT_ME(cerr);
					cerr << "Frame tag has no image!" << endl;
					new_frame.Image = NULL;
				} catch(...) {
					OUTPUT_ME(cerr);
					cerr << "Unable able to load image \"" << (cur_dir + image_name)
						 << "\" for animation frame" << endl;
					new_frame.Image = NULL;
				}

				// add frame if it is valid
				if(new_frame.Image && new_frame.Duration) {
					FrameList.push_back(new_frame);

					// build dimensions
					Width = std::max(Width, (unsigned int)new_frame.Image->GetWidth());
					Height = std::max(Height, (unsigned int)new_frame.Image->GetHeight());
				}
			}
		}
		
		// increment
		++xml_iter;
	}

	#undef OUTPUT_ME
}
//---------------------------------------------------------------------------  
void TAnimation2D::CopyFrom(const TAnimation2D& V) 
{
	// clear old data
	Clear();

	// copy frames
	FrameList.reserve(V.GetFrameCount());
	for(unsigned int i = 0; i < V.GetFrameCount(); i++) {
		Frame new_frame;
		new_frame.Duration = V.FrameList[i].Duration;
		new_frame.Image = new ksd::TImage(*V.FrameList[i].Image);

		FrameList.push_back(new_frame);
	}

	// copy dims
	Width = V.Width;
	Height = V.Height;
}
//---------------------------------------------------------------------------  
void TAnimation2D::DrawFrame(ksd::TCanvas* Canvas, unsigned int frame, TScreenPosition pos)
{
	if(frame < GetFrameCount()) {
		Canvas->Blit((int)pos.X, (int)pos.Y, *FrameList[frame].Image);
	}
}
//---------------------------------------------------------------------------  
bool TAnimationMonitor2D::Update()
{
	if(IsValid() && Running) {
		// get current delta
		Timer.Freeze();
		Time += Timer.GetDelta();
	
		if(Time >= Animation->GetDuration(Frame)) {
			// clear the offset from last frame
			Time = 0;

			// increment
			++Frame;

			// TEMP: for now just loop! Add control functions ...
			if(Frame > Animation->GetFrameCount() - 1) {
				Frame = 0;
			}
			
			return true;
		} 
	}

	return false;
}
//---------------------------------------------------------------------------  
void TAnimationMonitor2D::Draw(ksd::TCanvas* Canvas, TScreenPosition pos) 
{
	if(IsValid()) {
		Animation->DrawFrame(Canvas, Frame, pos);
	}
}
//---------------------------------------------------------------------------  
void TAnimationMonitor2D::Start()
{
	if(IsValid() && !Running) {
		Timer.Reset();
		Running = true;
	}
}
//---------------------------------------------------------------------------  
void TAnimationMonitor2D::Stop()
{
	if(IsValid() && Running) {
		// save offset
		Timer.Freeze();
		Time += Timer.GetDelta();

		// stop running
		Running = false;
	}
}
//---------------------------------------------------------------------------  

