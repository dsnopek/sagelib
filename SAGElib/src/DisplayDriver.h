//--------------------------------------------------------------------------- 
//	SAGElib -- Scene-Actor Game Engine
//	Copyright 2001 David Snopek
//	
//	This library is free software; you can redistribute it and/or modify
//	it under the terms of the GNU General Public License as published by
//	the Free Software Foundation; either version 2 of the License, or
//	(at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//
//	You should have received a copy of the GNU General Public License
//	along with this program; if not, write to the Free Software
//	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
//--------------------------------------------------------------------------- 
//	File:			$RCSfile$
//	
//	Author:			David Snopek
//	Revised by:		
//	Version:		$Revision$
//	
//	Objects:		TDisplayDriver
//	
//	Purpose:		Client display driver base class.
//	
//	Checked-in by:	$Author$
//					$Date$
//---------------------------------------------------------------------------
#ifndef _SAGE_DisplayDriverH_
#define _SAGE_DisplayDriverH_
//---------------------------------------------------------------------------
#include <ksd/Canvas.h>
#include "types.h"
//--------------------------------------------------------------------------- 
#ifdef __GNUG__
#pragma interface
#endif
//---------------------------------------------------------------------------
namespace SAGE {

// forward decls
namespace Driver2D {
	class IMedia2D;
};
//---------------------------------------------------------------------------
/** Displays objects communicated from the server into actual screen 
 ** objects.  The objects can be interpreted in any number of ways.  Overload 
 ** this class to create a new way of displaying them.
 */
class TDisplayDriver {
public:
	TDisplayDriver();
	virtual ~TDisplayDriver();

	/** Uses this media definition file to find the required media.  The media
	 ** must be of a format that this driver can understand.
	 */
	virtual void LoadMediaDefinition(const std::string& filename) = 0;

	virtual void CreateEntity(const std::string& name) = 0;
	virtual void DestroyEntity(const std::string& name) = 0;

	virtual void EntityDoAction(const std::string& entity, const std::string& action) = 0;
	virtual void EntityMove(const std::string& entity, TWorldPosition pos) = 0;

	/** Updates the currently displayed game output.  Returns true if anything was
	 ** actually drawn.
	 */
	virtual bool Update(ksd::TCanvas* Canvas) = 0;

	/** Re-draws the entire game display.
	 */
	virtual void Repaint(ksd::TCanvas* Canvas) = 0;

	/** Request a 2d media interface.  If this driver cannot provide one or
	 ** chooses not to, then return NULL.  This object should be deallocated
	 ** when you are done with it.
	 */
	virtual Driver2D::IMedia2D* GetMedia2DInterface() = 0;
};
//---------------------------------------------------------------------------  
}; // namespace SAGE
//---------------------------------------------------------------------------  
#endif

