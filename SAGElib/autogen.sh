#!/bin/bash

# forces use of new autotools under some distributions
export WANT_AUTOMAKE_1_5=1
#export WANT_AUTOCONF_2_5=1

# get info from autogen.rc
if test -f autogen.rc ; then
	. autogen.rc
fi

# used to set a variable if it is not set
# takes two arguments, the variable name and then the value
set_variable_default() {
	if test x$1 = x -o x$2 = x ; then
		echo "Invalid call to set_variable_default"
		exit 1
	fi
	if test x`eval echo \\\$\$1` = x ; then
		eval $1=$2
	fi
}
	
# Set the configurable variable defaults
set_variable_default RUN_ACLOCAL y
set_variable_default RUN_AUTOHEADER y
set_variable_default RUN_AUTOMAKE y
set_variable_default RUN_AUTOCONF y
set_variable_default ALWAYS_REBUILD n

AUTOGEN_TIMESTAMP="autogen.timestamp"
REBUILD=`if test -f $AUTOGEN_TIMESTAMP -o x$ALWAYS_REBUILD = xy ; then echo n ; else echo y ; fi`
CUR_DIR=`pwd`
SELF=$CUR_DIR/$0
LAYER=0

# used to draw an indent denoting that we are a layer deeper
show_indent() {
	COUNTER=$LAYER
	while test $COUNTER -gt 0 ; do
		echo -n "++"
		COUNTER=`expr $COUNTER - 1`
	done
}

# first deal with subdirs
if test x$SUBDIRS != x ; then
	for dir in $SUBDIRS ; do
		# tell the user that we are entering a new directory
		show_indent
		echo "->Entering $dir ..."

		# do the actual work
		cd $dir
		$SELF $@ --layer=`expr $LAYER + 1`

		# tell the user that we are leaving that directory
		show_indent
		echo "<-Leaving $dir ..."
		cd $CUR_DIR
	done
fi

# parse the args
for arg in $@ ; do
	case $arg in
		--run-aclocal) RUN_ACLOCAL=y;;
		--run-autoheader) RUN_AUTOHEADER=y;;
		--run-automake) RUN_AUTOMAKE=y;;
		--run-autoconf) RUN_AUTOCONF=y;;
		--run-libtoolize) RUN_LIBTOOLIZE=y;;
		--no-aclocal) RUN_ACLOCAL=n;;
		--no-autoheader) RUN_AUTOHEADER=n;;
		--no-automake) RUN_AUTOMAKE=n;;
		--no-autoconf) RUN_AUTOCONF=n;;
		--no-libtoolize) RUN_LIBTOOLIZE=n;;
		--only-aclocal) 
			RUN_ACLOCAL=y
			RUN_AUTOHEADER=n
			RUN_AUTOMAKE=n
			RUN_AUTOCONF=n
			;;
		--only-autoheader)
			RUN_ACLOCAL=n
			RUN_AUTOHEADER=y
			RUN_AUTOMAKE=n
			RUN_AUTOCONF=n
			;;
		--only-automake)
			RUN_ACLOCAL=n
			RUN_AUTOHEADER=n
			RUN_AUTOMAKE=y
			RUN_AUTOCONF=n
			;;
		--only-autoconf)
			RUN_ACLOCAL=n
			RUN_AUTOHEADER=n
			RUN_AUTOMAKE=n
			RUN_AUTOCONF=y
			;;
		--run-all)
			RUN_ACLOCAL=y
			RUN_AUTOHEADER=y
			RUN_AUTOMAKE=y
			RUN_AUTOCONF=y
			;;
		--rebuild) REBUILD=y ;;
		--layer=*)
			LAYER=`echo $arg | sed -e s,--layer=,,`
			;;
		*)
			echo Unknown arg: $arg
			exit 1
			;;
	esac
done

# deal with rebuild
AUTOMAKE_FLAGS=`if test x$REBUILD = xy ; then echo "-a -f" ; fi`
RUN_LIBTOOLIZE=$REBUILD

do_libtoolize() {
	if ! which libtoolize > /dev/null; then
		echo "*** Error: libtool is required to generate build scripts!"
		exit 1
	fi
	
	libtoolize --force 1> /dev/null
	RUN_ACLOCAL=y
}

do_aclocal() {
	if test `aclocal --print-ac-dir` != "/usr/local/share/aclocal" && \
	   test -d /usr/local/share/aclocal 
	then
		# only call aclocal on /usr/local/share if the system
		# requires it ...
		aclocal -I /usr/local/share/aclocal 
	else
		aclocal
	fi
}

do_autoheader() {
	autoheader
}

do_automake() {
	automake $AUTOMAKE_FLAGS --foreign --include-deps
}

do_autoconf() {
	autoconf
}

# Run libtoolize
show_indent
if test x$RUN_LIBTOOLIZE = xy ; then
	echo Running libtoolize ...
	do_libtoolize
else
	echo Skipping libtoolize ...
fi

# Run aclocal
show_indent
if test x$RUN_ACLOCAL = xy ; then
	echo Running aclocal ...
	do_aclocal
else
	echo Skipping aclocal ...
fi

# Run autoheader
show_indent
if test x$RUN_AUTOHEADER = xy ; then
	echo Running autoheader ...
	do_autoheader
else
	echo Skipping autoheader ...
fi


# Run automake
show_indent
if test x$RUN_AUTOMAKE = xy ; then
	echo Running automake ...
	do_automake
else
	echo Skipping automake ...
fi

# Run autoconf
show_indent
if test x$RUN_AUTOCONF = xy ; then
	echo Running autoconf ...
	do_autoconf
else
	echo Skipping autoconf ...
fi

# mark as run
date > $AUTOGEN_TIMESTAMP

#./configure $*
show_indent
echo "Now you are ready to run ./configure"

